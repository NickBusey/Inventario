<?php
namespace App\Event;
use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;

class RoomsListener implements EventListenerInterface {

    public function implementedEvents() {
        return array(
            'Model.Assets.beforeAdd' => 'handleAddAsset',
        );
    }

    public function handleAddAsset($event, $asset) {
        $this->Rooms = TableRegistry::get('Rooms');

        if ($asset['room_title']) {
            $room = $this->Rooms->newEntity();
            $room->title = $asset['room_title'];
            $room->user_id = $asset['user_id'];
            $this->Rooms->save($room);
            $asset->room_id = $room->id;
            $event->setResult(['asset' => $asset]);
        }
    }
}
