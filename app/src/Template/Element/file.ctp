
<div class="col-lg-4 col-md-4 col-sm-4 mb">
  <div class="content-panel pn">
    <div id="spotify" style="background-image: url('/<?=$file->dir?><?=$file->photo?>');">
      <div class="col-xs-4 col-xs-offset-8">
        <a href="/<?=$file->dir?><?=$file->photo?>" class="btn btn-sm btn-clear-g">OPEN</a>
      </div>
      <div class="play">
        <?= $this->Form->postLink(__('Delete'), ['controller'=>'Files','action' => 'delete', $file->id], ['class'=>'btn btn-sm','confirm' => __('Are you sure you want to delete # {0}?', $file->id)]) ?>
      </div>
    </div>
    <p class="followers">
      <?php if ($file->type == $this->Enum->enumValueToKey('Files','type','Photo')) { ?>
        <i class="fa fa-camera"></i> Photo
      <?php } else if ($file->type == $this->Enum->enumValueToKey('Files','type','Manual')) { ?>
        <i class="fa fa-book"></i> Manual
      <?php } else if ($file->type == $this->Enum->enumValueToKey('Files','type','Warranty')) { ?>
        <i class="fa fa-file-invoice"></i> Warranty
      <?php } else if ($file->type == $this->Enum->enumValueToKey('Files','type','Receipt')) { ?>
        <i class="fa fa-receipt"></i> Receipt
      <?php } ?>
    </p>
  </div>
</div>
