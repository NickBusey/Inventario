<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Asset $asset
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Asset'), ['action' => 'edit', $asset->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Asset'), ['action' => 'delete', $asset->id], ['confirm' => __('Are you sure you want to delete # {0}?', $asset->id)]) ?> </li>
        <li><?= $this->Html->link(__('New Asset'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="assets view large-9 medium-8 columns content">
    <h3><?= h($asset->title) ?></h3>
    <img src="<?=$asset->image_url?>" width=100 />
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($asset->title) ?></td>
        </tr>
        <?php if ($asset->price_paid) { ?>
            <tr>
                <th scope="row"><?= __('Price Paid') ?></th>
                <td><?= $this->Number->currency($asset->price_paid,'USD') ?></td>
            </tr>
        <?php } ?>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Enum->enumKeyToValue('Assets','status',$asset->status) ?></td>
        </tr>
        <?php if ($asset->room) { ?>
            <tr>
                <th scope="row"><?= __('Room') ?></th>
                <td><?php echo $this->Html->link($asset->room->title, ['action' => 'view', 'controller'=>'Rooms', $asset->room->id]); ?></td>
            </tr>
        <?php } ?>
        <?php if ($asset->container) { ?>
            <tr>
                <th scope="row"><?= __('Container') ?></th>
                <td><?php echo $this->Html->link($asset->container->title, ['action' => 'view', 'controller'=>'Containers', $asset->container->id]); ?></td>
            </tr>
        <?php } ?>
        <?php if ($asset->manufacturer) { ?>
            <tr>
                <th scope="row"><?= __('Manufacturer') ?></th>
                <td><?=h($asset->manufacturer); ?></td>
            </tr>
        <?php } ?>
        <?php if ($asset->model) { ?>
            <tr>
                <th scope="row"><?= __('Model') ?></th>
                <td><?=h($asset->model); ?></td>
            </tr>
        <?php } ?>
        <?php if ($asset->serial) { ?>
            <tr>
                <th scope="row"><?= __('Serial') ?></th>
                <td><?=h($asset->serial); ?></td>
            </tr>
        <?php } ?>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($asset->description)); ?>
    </div>
    <div class="row">
        <h4><?= __('Photos and Files') ?></h4>
        <?php foreach ($asset->files as $file) { ?>
            <?=$this->element('file',['file'=>$file])?>
        <?php } ?>
 
    </div>
    <?= $this->Form->create($file, ['url'=>'/files/add','type' => 'file','id'=>'fileUpload']) ?>
        <a id="fileUploadButton" href="#" class="btn btn-sm btn-clear-g">Upload</a>
            <?php
                echo $this->Form->control('photo', ['type' => 'file','style'=>'visibility: hidden;','label'=>false]);
                echo $this->Form->control('type', ['options' => $this->Enum->selectValues('Files', 'type')]);
                echo $this->Form->hidden('asset_id', ['type' => 'input', 'value' => $asset->id]);
            ?>
        <?= $this->Form->button(__('Submit'),['hidden'=>true]) ?>
        <?= $this->Form->end() ?>
</div>
