<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Asset[]|\Cake\Collection\CollectionInterface $assets
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Asset'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Import'), ['action' => 'import']) ?></li>
    </ul>
</nav>
<div class="assets index large-9 medium-8 columns content">

    <div class="search-box pull-right" style="margin-bottom: 10px;">
        <?php
        echo $this->Form->create(null, ['valueSources' => 'query']);
        echo $this->Form->control('title', ['placeholder' => 'Wildcards: * and ?']);
        echo $this->Form->control('tag', ['options' => ['' => '- does not matter -', '-1' => '- all without any tag -'] + $tags]);
        echo $this->Form->button(__('Search'), ['class' => 'btn btn-primary']);
        if (!empty($_isSearch)) {
            echo ' ';
            echo $this->Html->link(__('Reset'), ['action' => 'index', '?' => array_intersect_key($this->request->getQuery(), array_flip(['sort', 'direction']))], ['class' => 'btn btn-default']);
        }
        echo $this->Form->end();
        ?>
</div>
    <h3><?= __('Assets') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Tags') ?></th>
                <th scope="col"><?= $this->Paginator->sort('price_paid') ?></th>
                <th scope="col"><?= $this->Paginator->sort('found_price_median') ?></th>
                <th scope="col"><?= $this->Paginator->sort('container_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('room_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($assets as $asset): ?>
            <tr>
                <td>
                <?php 
                $output = "";
                if ($asset->image_url) {
                    $output .= "<img src=".$asset->image_url." width=100 /> ";
                }
                $output .= $asset->title;
                echo $this->Html->link($output, ['action' => 'view', $asset->id],['escape' => false]) ?></td>
                
                <td><?= $this->Tags->displayTags($asset->tags) ?></td>
                <td><?= $this->Number->format($asset->price_paid) ?></td>
                <td><?= $this->Number->format($asset->found_price_median) ?></td>
                <td><?php if ($asset->container) { echo $this->Html->link($asset->container->title, ['action' => 'view', 'controller'=>'Containers', $asset->container->id]); } ?></td>
                <td><?php if ($asset->room) { echo $this->Html->link($asset->room->title, ['action' => 'view', 'controller'=>'Rooms', $asset->room->id]); } ?></td>
                <td><?= $this->Enum->enumKeyToValue('Assets','status',$asset->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $asset->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $asset->id], ['confirm' => __('Are you sure you want to delete # {0}?', $asset->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
