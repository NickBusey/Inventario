<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Room $room
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Room'), ['action' => 'edit', $room->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Room'), ['action' => 'delete', $room->id], ['confirm' => __('Are you sure you want to delete # {0}?', $room->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Rooms'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Room'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Assets'), ['controller' => 'Assets', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Asset'), ['controller' => 'Assets', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="rooms view large-9 medium-8 columns content">
    <h3><?= h($room->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($room->title) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($room->description)); ?>
    </div>
    <div class="row">
        <h4><?= __('Photos') ?></h4>
        <?php foreach ($room->files as $file) { ?>
            <?=$this->element('file',['file'=>$file])?>
        <?php } ?>
 
    </div>
    <?= $this->Form->create($file, ['url'=>'/files/add','type' => 'file','id'=>'fileUpload']) ?>
        <a id="fileUploadButton" href="#" class="btn btn-sm btn-clear-g">Upload</a>
            <?php
                echo $this->Form->control('photo', ['type' => 'file','style'=>'visibility: hidden;','label'=>false]);
                echo $this->Form->hidden('type', ['value' => $this->Enum->enumValueToKey('Files', 'type','Photo')]);
                echo $this->Form->hidden('room_id', ['type' => 'input', 'value' => $room->id]);
            ?>
        <?= $this->Form->button(__('Submit'),['hidden'=>true]) ?>
        <?= $this->Form->end() ?>
    <div class="related">
        <h4><?= __('Related Assets') ?></h4>
        <?php if (!empty($room->assets)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Manufacturer Id') ?></th>
                <th scope="col"><?= __('Price Paid') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Author') ?></th>
                <th scope="col"><?= __('Found Price High') ?></th>
                <th scope="col"><?= __('Found Price Low') ?></th>
                <th scope="col"><?= __('Found Price Median') ?></th>
                <th scope="col"><?= __('Quantity') ?></th>
                <th scope="col"><?= __('Room Id') ?></th>
                <th scope="col"><?= __('Container Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($room->assets as $assets): ?>
            <tr>
                <td><?= h($assets->title) ?></td>
                <td><?= h($assets->manufacturer_id) ?></td>
                <td><?= h($assets->price_paid) ?></td>
                <td><?= h($assets->status) ?></td>
                <td><?= h($assets->description) ?></td>
                <td><?= h($assets->author) ?></td>
                <td><?= h($assets->found_price_high) ?></td>
                <td><?= h($assets->found_price_low) ?></td>
                <td><?= h($assets->found_price_median) ?></td>
                <td><?= h($assets->quantity) ?></td>
                <td><?= h($assets->room_id) ?></td>
                <td><?= h($assets->container_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Assets', 'action' => 'view', $assets->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Assets', 'action' => 'edit', $assets->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Assets', 'action' => 'delete', $assets->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assets->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Containers') ?></h4>
        <?php if (!empty($room->containers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Room Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($room->containers as $containers): ?>
            <tr>
                <td><?= h($containers->title) ?></td>
                <td><?= h($containers->description) ?></td>
                <td><?= h($containers->room_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Containers', 'action' => 'view', $containers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Containers', 'action' => 'edit', $containers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Containers', 'action' => 'delete', $containers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $containers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
