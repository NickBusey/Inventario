<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;

class TagsHelper extends Helper
{
  public $helpers = ['Html'];

  public function displayTags($tags) {
    $out = "";
    foreach ($tags as $tag) {
      $out .= $this->Html->link($tag['label'], ['action' => 'index', '?' => ['tag' => $tag['slug']]]).", ";
    }
    return substr($out,0,strlen($out)-2);
  }
}