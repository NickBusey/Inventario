<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * File Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 * @property int|null $asset_id
 * @property string|null $photo
 * @property string|null $dir
 * @property string|null $size
 * @property int|null $type
 * @property string|null $file_type
 * @property int|null $container_id
 * @property int|null $room_id
 *
 * @property \App\Model\Entity\Asset $asset
 * @property \App\Model\Entity\Container $container
 * @property \App\Model\Entity\Room $room
 */
class File extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'asset_id' => true,
        'photo' => true,
        'dir' => true,
        'size' => true,
        'type' => true,
        'file_type' => true,
        'container_id' => true,
        'room_id' => true,
        'asset' => true,
        'container' => true,
        'room' => true
    ];
}
