<?php
namespace App\Controller;

use App\Controller\AppController;
use MarcL\AmazonAPI;
use MarcL\AmazonUrlBuilder;
use Cake\Utility\Hash;
use Cake\Mailer\Email;

/**
 * Assets Controller
 *
 * @property \App\Model\Table\AssetsTable $Assets
 *
 * @method \App\Model\Entity\Asset[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AssetsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    
        $this->loadComponent('Search.Prg');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'order' => [
                'Assets.created DESC',
            ],
            'contain' => ['Rooms','Containers','Tags'],
            'conditions' => ['Assets.user_id' => $this->request->getSession()->read('Auth.User.id')]
        ];
        $query = $this->Assets->find('search', ['search' => $this->request->getQuery()])->contain(['Tags']);
        $assets = $this->paginate($query);

        $tags = $this->Assets->Tagged->find()->distinct(['Tags.slug', 'Tags.label'])->contain(['Tags'])->toArray();
        $tags = Hash::combine($tags, '{n}.tag.slug', '{n}.tag.label');

        $this->set(compact('assets','tags'));
    }

    /**
     * View method
     *
     * @param string|null $id Asset id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $asset = $this->Assets->get($id, [
            'contain' => ['Tags','Files','Rooms','Containers']
        ]);

        $this->loadModel('Files');
        $file = $this->Files->newEntity();

        $this->set(compact('asset', 'file'));
    }

    public function add()
    {
        $this->loadModel('Assets');
        $this->loadModel('Rooms');
        $this->loadModel('Containers');

        $asset = $this->Assets->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['user_id'] = $this->request->getSession()->read('Auth.User.id');

            $asset = $this->Assets->addAsset($data);
            if ($asset->created == $asset->modified) {
                $isNew = true;
            } else {
                $isNew = false;
            }
            # Return the price the user entered, or return the median found price.
            $median = ($asset->price_found_median ? $asset->price_found_median : 0);
            $price = ($data['amount_paid'] ? $data['amount_paid'] : $median);
            $response = [
                "asset_id"=>$asset->id,
                "title"=>$asset->title,
                "image_url"=>$asset->image_url,
                "new"=>$isNew,
                "price"=>$price
            ];
            if (isset($room->id)) {
                $response['room_title'] = $asset->room_title;
                $response['room_id'] = $asset->room_id;
            }
            if (isset($container->id)) {
                $response['container_title'] = $asset->container_title;
                $response['container_id'] = $asset->container_id;
            }
            return $this->response->withType("application/json")->withStringBody(json_encode($response));
        } else {
            $rooms = $this->Rooms->find('list', [
                'limit' => 200,
                'conditions' => ['Rooms.user_id' => $this->request->getSession()->read('Auth.User.id')]
            ]);
            $tagged = $this->Assets->Tagged->find('cloud')->toArray();
            $this->set(compact('rooms','asset','tagged'));
        }

    }

    /**
     * Edit method
     *
     * @param string|null $id Asset id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('Containers');
        $this->loadModel('Rooms');
        $asset = $this->Assets->get($id, [
            'contain' => ['Tags']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $asset = $this->Assets->patchEntity($asset, $this->request->getData());
            if ($this->Assets->save($asset)) {
                $this->Flash->success(__('The asset has been saved.'));

                return $this->redirect(['action' => 'view',$asset->id]);
            }
            $this->Flash->error(__('The asset could not be saved. Please, try again.'));
        }
        $tags = $this->Assets->Tags->find('list', ['keyField' => 'slug']);
        $rooms = $this->Rooms->find('list', [
            'limit' => 200,
            'conditions' => ['Rooms.user_id' => $this->request->getSession()->read('Auth.User.id')]
        ]);
        $containers = $this->Containers->find('list', [
            'limit' => 200,
            'conditions' => ['Containers.user_id' => $this->request->getSession()->read('Auth.User.id')]
        ]);
        $this->set(compact('asset', 'tags', 'rooms', 'containers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Asset id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $asset = $this->Assets->get($id);
        if ($this->Assets->delete($asset)) {
            $this->Flash->success(__('The asset has been deleted.'));
        } else {
            $this->Flash->error(__('The asset could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function import()
    {
        $this->loadModel('Rooms');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $asset = $this->Assets->import($this->request->getData('text'),$this->request->getSession()->read('Auth.User.id'));
            return ($this->redirect(['action' => 'index']));
        } else {
            $rooms = $this->Rooms->find('list', [
                'limit' => 200,
                'conditions' => ['Rooms.user_id' => $this->request->getSession()->read('Auth.User.id')]
            ]);
            $tagged = $this->Assets->Tagged->find('cloud')->toArray();
            $this->set(compact('rooms','asset','tagged'));
        }
    }
}
