<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Containers Controller
 *
 * @property \App\Model\Table\ContainersTable $Containers
 *
 * @method \App\Model\Entity\Container[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContainersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Rooms'],
            'conditions' => ['Containers.user_id' => $this->request->getSession()->read('Auth.User.id')]
        ];
        $containers = $this->paginate($this->Containers);

        $this->set(compact('containers'));
    }

    /**
     * View method
     *
     * @param string|null $id Container id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $container = $this->Containers->get($id, [
            'contain' => ['Rooms', 'Assets', 'Files']
        ]);

        $this->loadModel('Files');
        $file = $this->Files->newEntity();

        $this->set(compact('container','file'));
    }

    public function getByRoom() {
        if ($this->request->is('post')) {
            $roomId = $this->request->getData(['room_id']);
            $this->paginate = [
                'conditions' => [
                    // 'Containers.user_id' => $this->request->session()->read('Auth.User.id'),
                    'Containers.room_id' => $roomId
                ],
                'fields' => [
                    'Containers.id',
                    'Containers.title'
                ]
            ];
            $containers = $this->paginate($this->Containers);

            return $this->response->withType("application/json")->withStringBody(json_encode($containers));
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $container = $this->Containers->newEntity();
        if ($this->request->is('post')) {
            $container = $this->Containers->patchEntity($container, $this->request->getData());
            $container->user_id = $this->request->getSession()->read('Auth.User.id');
            if ($this->Containers->save($container)) {
                $this->Flash->success(__('The container has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The container could not be saved. Please, try again.'));
        }
        $rooms = $this->Containers->Rooms->find('list', [
            'limit' => 200,
            'conditions' => ['Rooms.user_id' => $this->request->getSession()->read('Auth.User.id')]
        ]);
        if ($rooms->count() < 1) {
            $this->Flash->success(__('You must create a room before you create a container.'));

            return $this->redirect(['controller' => 'Rooms', 'action' => 'add']);
        }
        $this->set(compact('container', 'rooms'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Container id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $container = $this->Containers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $container = $this->Containers->patchEntity($container, $this->request->getData());
            if ($this->Containers->save($container)) {
                $this->Flash->success(__('The container has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The container could not be saved. Please, try again.'));
        }
        $rooms = $this->Containers->Rooms->find('list', ['limit' => 200]);
        $this->set(compact('container', 'rooms'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Container id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $container = $this->Containers->get($id);
        if ($this->Containers->delete($container)) {
            $this->Flash->success(__('The container has been deleted.'));
        } else {
            $this->Flash->error(__('The container could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
