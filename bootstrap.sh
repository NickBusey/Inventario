mkdir data
docker-compose up -d
docker exec -it $(docker ps -q -f name=inventario_1) composer update && composer install
docker exec -it $(docker ps -q -f name=inventario_1) bin/cake migrations migrate
docker exec -it $(docker ps -q -f name=inventario_1) bin/cake migrations seed
docker exec -it $(docker ps -q -f name=inventario_1) bin/cake cache clear _cake_model_
